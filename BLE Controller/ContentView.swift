//
//  ContentView.swift
//  BLE Controller
//
//  Created by 池田 和広 on 2019/09/29.
//  Copyright © 2019 Kazuhiro Ikeda. All rights reserved.
//

import Combine
import SwiftUI
import CoreBluetooth

struct ContentView: View {
//    @State var sliderValue = 0.0
//    var minimumValue = 0.0
//    var maximumVelue = 100.0
    
    @State var ble: BluetoothService = BluetoothService()
    
      var body: some View {
        VStack {
            VStack {
//                Toggle(isOn: $bleManager.isEnabled) {
//                    Text("The value is " + "\(bleManager.isEnabled)")
//                }
//                VStack {
//                    Text("\(String(format: "%06.2f", Double(sliderValue)))")
//
//                        .foregroundColor(Color.red)
//                        .multilineTextAlignment(.trailing)
//
//                    HStack {
//                        Text("\(Int(minimumValue))")
//                        Slider(value: $sliderValue, in: minimumValue...maximumVelue)
//                        Text("\(Int(maximumVelue))")
//                    }
//                }
                Text("BLE LED")
                    .font(.largeTitle)
                    .padding(20)
                HStack{
                    Button(action: {
                        self.ble.sendMessage(message: "1")
//                        self.sliderValue = 0
                       }) {
                        HStack {
                            Text("ON")
                                .font(.title)
                        }
                        .frame(width: 150, height: 100, alignment: .center)
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(color: Color.gray, radius: 5, x: 5, y: 5)
                      }
                   
                    Button(action: {
                        self.ble.sendMessage(message: "0")
//                     self.sliderValue = 50
                    }) {
                        HStack {
                            Text("OFF")
                                .font(.title)
                        }
                        .frame(width: 150, height: 100, alignment: .center)
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(color: Color.gray, radius: 5, x: 5, y: 5)
                    }
                }
            }.padding(10)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class BluetoothService: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {

        switch (central.state) {
        case .unknown :
            print("state : unknown")
            
        case .resetting:
            print("state : resetting")
            
        case .unsupported:
            print("state : unsurpported")
            
        case .unauthorized:
            print("state : unauthorized")
            
        case .poweredOff:
            print("state : poweredOff")
            
        case .poweredOn:
            print("state : poweredOn")
            scanStart()

        @unknown default:
            print("state : default")
            
        }
    }
    
    var centralManager: CBCentralManager?
    
    override init() {
        super.init()
        print("state : manager init")
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
   public func scanStart() {
        print("state : BLE scan start")
        if centralManager!.isScanning == false {
            centralManager!.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    var peripherals: [CBPeripheral] = []
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("state : discovered \(peripheral.name)")
        peripherals.append(peripheral)
        connect()
    }
    
    var cbPeripheral: CBPeripheral? = nil
    
    func connect() {
        for peripheral in peripherals {
            if peripheral.name != nil && peripheral.name == "GENUINO 101-2D9C" {
                cbPeripheral = peripheral
                centralManager?.stopScan()
                print("state : found \(String(describing: peripheral.name))")
                break
            }
        }
        
        if cbPeripheral != nil {
            centralManager!.connect(cbPeripheral!, options: nil)
            print("state : connect to \(cbPeripheral?.name)")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        let services: [CBUUID] = [CBUUID(string: "19B10000-E8F2-537E-4F6C-D104768A1214")]
        cbPeripheral!.discoverServices(services)
        print("state : service scanning")

    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("BLE connection failed.")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        let serviceUUID: CBUUID = CBUUID(string: "19B10000-E8F2-537E-4F6C-D104768A1214")
        for service in cbPeripheral!.services! {
            if service.uuid == serviceUUID {
                cbPeripheral?.discoverCharacteristics(nil, for: service)
                 print("state : charactoeristics scanning")
            }
        }
    }
    
    var writeCharacteristic: CBCharacteristic? = nil
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            if characteristic.uuid.uuidString == "NotifyUUID" {
                peripheral.setNotifyValue(true, for: characteristic)
                print("state : Set NOTIFY to \(characteristic.uuid.uuidString)")
            }
            
            if characteristic.uuid.uuidString == "19B10001-E8F2-537E-4F6C-D104768A1214" {
                writeCharacteristic = characteristic
                print("state : Find WRITE \(characteristic.uuid.uuidString)")
            }
        }
    }
    
    func sendMessage (message: String) {
        var command = message
        let data = command.data(using: String.Encoding.utf8, allowLossyConversion: true)
         print("state : Sended message \(message)")
        print("state : Sended data \(String(describing: data))")
        cbPeripheral!.writeValue(data!, for: writeCharacteristic!, type: .withResponse)
    }
}
